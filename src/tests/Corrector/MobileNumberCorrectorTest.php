<?php
declare(strict_types=1);

namespace App\Test;

use App\Corrector\MobileNumberCorrector;
use PHPUnit\Framework\TestCase;

class MobileNumberCorrectorTest extends TestCase
{
    /**
     * @var MobileNumberCorrector
     */
    private $corrector;

    protected function setUp()
    {
        $this->corrector = new MobileNumberCorrector('27', 9, ['6', '7', '8']);
    }

    public function test_rejects_empty_number()
    {
        $this->assertIncorrect('');
        $this->assertPreservesOriginalNumber('');
    }

    public function test_rejects_very_short_number()
    {
        $this->assertIncorrect('12345678');
        $this->assertPreservesOriginalNumber('12345678');
    }

    public function test_rejects_very_long_number()
    {
        $this->assertIncorrect('123456789123');
        $this->assertPreservesOriginalNumber('123456789123');
    }

    public function test_rejects_incorrect_mobile_prefix()
    {
        $this->assertIncorrect('27123456789');
        $this->assertPreservesOriginalNumber('27123456789');
    }

    public function test_incorrect_number_with_valid_prefix()
    {
        $this->assertIncorrect('6478342944');
        $this->assertPreservesOriginalNumber('6478342944');
    }

    public function test_correct_number()
    {
        $this->assertTrue(
            $this->corrector->correct('27823456789')->isCorrect()
        );
        $this->assertPreservesOriginalNumber('27823456789');
    }

    public function test_corrects_non_numerics()
    {
        $result = $this->corrector->correct('String_27823456789string');

        $this->assertTrue($result->isCorrected());
        $this->assertEquals('27823456789', $result->getCorrectedNumber());
        $this->assertEquals('removed non numeric characters', $result->getCorrection());
        $this->assertPreservesOriginalNumber('String_27823456789string');
    }

    public function test_corrects_missing_calling_code()
    {

        $result = $this->corrector->correct('823456789');

        $this->assertTrue($result->isCorrected());
        $this->assertEquals('27823456789', $result->getCorrectedNumber());
        $this->assertEquals('added calling code', $result->getCorrection());
        $this->assertPreservesOriginalNumber('823456789');
    }

    public function test_corrects_non_numeric_and_missing_calling_code()
    {
        $result = $this->corrector->correct('string_823456789');

        $this->assertTrue($result->isCorrected());
        $this->assertEquals('27823456789', $result->getCorrectedNumber());
        $this->assertEquals('removed non numeric characters, added calling code', $result->getCorrection());
        $this->assertPreservesOriginalNumber('string_823456789');
    }

    public function test_correct_list()
    {
        $this->assertEquals(
            [
                $this->corrector->correct("1"),
                $this->corrector->correct("2")
            ],
            iterator_to_array($this->corrector->correctList(["1", "2"]))
        );
    }

    private function assertIncorrect($originalNumber): void
    {
        $this->assertTrue(
            $this->corrector->correct($originalNumber)->isInCorrect()
        );
    }

    private function assertPreservesOriginalNumber($originalNumber)
    {
        $this->assertEquals(
            $originalNumber, $this->corrector->correct($originalNumber)->getOriginalNumber()
        );
    }
}
