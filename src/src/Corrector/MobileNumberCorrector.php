<?php
declare(strict_types=1);

namespace App\Corrector;

class MobileNumberCorrector
{
    const REMOVED_NON_NUMERICS = 'removed non numeric characters';
    const ADDED_CALLING_CODE = 'added calling code';

    /** @var string */
    private $callingCode;
    /** @var int */
    private $nationalNumberLength;
    /** @var array */
    private $mobilePrefixes = [];

    public function __construct($callingCode, $nationalNumberLength, array $mobilePrefixes)
    {
        $this->callingCode = $callingCode;
        $this->nationalNumberLength = $nationalNumberLength;
        $this->mobilePrefixes = $mobilePrefixes;
    }

    public function correctList(array $originalNumbers) {
        foreach ($originalNumbers as $number) {
            yield $this->correct($number);
        }
    }

    public function correct(string $originalNumber): CorrectionResult
    {
        $corrections = [];

        $number = $this->removeNonNumericCharacters($originalNumber);

        if ($number !== $originalNumber) {
            $corrections[] = self::REMOVED_NON_NUMERICS;
        }

        if (!$this->hasValidLength($number)) {
            return new CorrectionResult($originalNumber);
        }

        if ($this->isNationalFormat($number)) {
            $number = $this->callingCode . $number;
            $corrections[] = self::ADDED_CALLING_CODE;
        }

        if (!$this->hasCorrectMobilePrefix($number)) {
            return new CorrectionResult($originalNumber);
        }

        if ($number === $originalNumber) {
            return new CorrectionResult($originalNumber, CorrectionResult::RESULT_CORRECT);
        }

        return new CorrectionResult($originalNumber, CorrectionResult::RESULT_CORRECTED, $number, implode(', ', $corrections));
    }

    private function removeNonNumericCharacters(string $originalNumber): string
    {
        return preg_replace('/^((?!\d).)*(\d*)((?!\d).)*/i', '$2', $originalNumber);
    }

    private function hasValidLength(string $number): bool
    {
        return
            strlen($number) === $this->nationalNumberLength ||
            strlen($number) === $this->nationalNumberLength + strlen($this->callingCode);
    }

    private function hasCorrectMobilePrefix($number): bool
    {
        $callingCodeLength = strlen($this->callingCode);
        foreach ($this->mobilePrefixes as $prefix) {
            if (strpos($number, $prefix, $callingCodeLength) === $callingCodeLength) {
                return true;
            }
        }
        return false;
    }

    private function isNationalFormat(string $number): bool
    {
        return strlen($number) === $this->nationalNumberLength;
    }
}
