<?php
declare(strict_types=1);

namespace App\Corrector;

class CorrectionResult
{
    const RESULT_CORRECT = 0;
    const RESULT_INCORRECT = 1;
    const RESULT_CORRECTED = 2;

    /**
     * @var string
     */
    private $originalNumber;
    /**
     * @var int
     */
    private $result;
    /**
     * @var string
     */
    private $correctedNumber;
    /**
     * @var string
     */
    private $correction;

    public function __construct($originalNumber, $result = self::RESULT_INCORRECT, $correctedNumber = '', $correction = '')
    {
        $this->originalNumber = $originalNumber;
        $this->result = $result;
        $this->correctedNumber = $correctedNumber;
        $this->correction = $correction;
    }

    public function isCorrect(): bool
    {
        return $this->result === self::RESULT_CORRECT;
    }

    public function isIncorrect(): bool
    {
        return $this->result === self::RESULT_INCORRECT;
    }

    public function isCorrected(): bool
    {
        return $this->result === self::RESULT_CORRECTED;
    }

    /**
     * @return string
     */
    public function getOriginalNumber(): string
    {
        return $this->originalNumber;
    }

    /**
     * @return string
     */
    public function getCorrectedNumber(): string
    {
        return $this->correctedNumber;
    }

    /**
     * @return string
     */
    public function getCorrection(): string
    {
        return $this->correction;
    }
}
