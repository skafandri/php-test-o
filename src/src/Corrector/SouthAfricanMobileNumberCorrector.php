<?php
declare(strict_types=1);

namespace App\Corrector;

class SouthAfricanMobileNumberCorrector extends MobileNumberCorrector
{
    /**
     * @link https://en.wikipedia.org/wiki/Telephone_numbers_in_South_Africa
     */
    public function __construct()
    {
        parent::__construct('27', 9, ['6', '7', '8']);
    }
}
