<?php
declare(strict_types=1);

namespace App\Controller;

use App\Corrector\SouthAfricanMobileNumberCorrector;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SouthAfricanMobileNumberCorrectorController extends AbstractController
{
    /**
     * @var SouthAfricanMobileNumberCorrector
     */
    private $corrector;

    public function __construct(SouthAfricanMobileNumberCorrector $corrector, LoggerInterface $logger)
    {
        $this->corrector = $corrector;
    }

    /**
     * @Route("/correct-number", name="correct-number")
     * @param Request $request
     * @return Response
     */
    public function correctSingleNumber(Request $request): Response
    {
        $form = $this->createCorrectNumberForm($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $number = $form->getData()['originalNumber'];
            return $this->renderCorrectNumberForm($form, $this->correctNumber($number));
        }

        return $this->renderCorrectNumberForm($form);
    }

    /**
     * @Route("/correct-numbers-csv", name="correct-numbers-csv")
     * @param Request $request
     * @return Response
     */
    public function correctNumbersCsv(Request $request): Response
    {
        $form = $this->createCorrectNumbersForm($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form->getData()['file'];
            $filePath = $file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename();
            $resultFilePath = tempnam(sys_get_temp_dir(), 'results-') . '.csv';
            try {
                $this->correctNumbersFromCsvToCsv($filePath, $resultFilePath);
                return $this->file($resultFilePath);
            } catch (\Exception $exception) {
                return Response::create(
                    sprintf('Cannot process file "%s"', $exception->getMessage()),
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
        }

        return $this->renderCorrectNumbersForm($form);
    }

    private function correctNumbersFromCsvToCsv(string $source, string $destination)
    {
        $resultFile = fopen($destination, 'w');
        fputcsv($resultFile, ['id', 'sms_phone', 'correction_result']);

        $handle = fopen($source, "r");
        //Skip header
        fgetcsv($handle);
        while (($line = fgetcsv($handle)) !== false) {
            fputcsv($resultFile, [$line[0], $line[1], $this->correctNumber($line[1])]);
        }
        fclose($handle);
        fclose($resultFile);
    }


    /**
     * @param string $number
     * @return string
     */
    private function correctNumber(string $number): string
    {
        $correction = $this->corrector->correct($number);
        switch (true) {
            case $correction->isCorrect();
                return 'correct';
            case $correction->isIncorrect();
                return 'incorrect';
            case $correction->isCorrected();
                return sprintf(' was corrected to "%s", %s', $correction->getCorrectedNumber(), $correction->getCorrection());
        }
    }

    /**
     * @param Request $request
     * @return FormInterface
     */
    private function createCorrectNumberForm(Request $request): FormInterface
    {
        $form = $this->createFormBuilder()
            ->add('originalNumber', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Correct single number'))
            ->getForm();

        $form->handleRequest($request);
        return $form;
    }

    /**
     * @param Request $request
     * @return FormInterface
     */
    private function createCorrectNumbersForm(Request $request): FormInterface
    {
        $form = $this->createFormBuilder()
            ->add('file', FileType::class)
            ->add('save', SubmitType::class, array('label' => 'Upload'))
            ->getForm();

        $form->handleRequest($request);
        return $form;
    }

    /**
     * @param FormInterface $form
     * @param string $number
     * @return Response
     */
    private function renderCorrectNumberForm(FormInterface $form, string $number = ''): Response
    {
        return $this->render('correct-number.html.twig', [
            'form' => $form->createView(),
            'correctionResult' => $number
        ]);
    }

    /**
     * @param FormInterface $form
     * @param string $number
     * @return Response
     */
    private function renderCorrectNumbersForm(FormInterface $form, string $number = ''): Response
    {
        return $this->render('correct-number.html.twig', [
            'form' => $form->createView(),
            'correctionResult' => $number
        ]);
    }
}
