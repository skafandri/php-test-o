## Mobile number corrector

This project uses the [Symfony](https://symfony.com/) framework version 4.

## Setup
You need to install [Docker](https://docs.docker.com/install/) and have a working `shell`.

Install the development environment
````bash
./bin/install.sh
````

## Test
Run the test suite
````bash
./bin/test.sh
````

## Run
Run the application with development environment in interactive mode
````bash
./bin/run.sh
````

Navigate to [http://localhost:8000](http://localhost:8000)
