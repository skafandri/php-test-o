#!/usr/bin/env sh

docker run --rm -it -u `id -u $USER` -v $(pwd)/src:/var/www/html --name php-test -p 8000:8000 php-test
