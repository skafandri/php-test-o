#!/usr/bin/env sh

docker run -u `id -u $USER` -v $(pwd)/src:/var/www/html php-test ./bin/phpunit
