#!/usr/bin/env sh

docker build -t php-test .

docker run -u `id -u $USER` -v $(pwd)/src:/var/www/html php-test composer install
