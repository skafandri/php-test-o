FROM php:7.2

RUN apt-get update && apt-get install -y unzip

RUN curl https://getcomposer.org/installer | php -- --filename=composer --install-dir=/bin
RUN apt-get update && apt-get install -y git zlib1g-dev
RUN docker-php-ext-install zip

COPY src/php.ini /usr/local/etc/php/

WORKDIR /var/www/html/

CMD php -S 0.0.0.0:8000 -t public
